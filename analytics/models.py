from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Analytic(models.Model):
    continent = models.CharField(max_length=50, blank=True, null=True)
    country = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    capital = models.CharField(max_length=50, blank=True, null=True)
    datetime = models.DateField(max_length=50, blank=True, null=True)
    ip = models.CharField(max_length=50, blank=True, null=True)

    os = models.CharField(max_length=100, blank=True, null=True)
    device = models.CharField(max_length=100, blank=True, null=True)
    type_device = models.CharField(max_length=100, blank=True, null=True)
    browser = models.CharField(max_length=100, blank=True, null=True)
    language= models.CharField(max_length=100, blank=True, null=True)
    country_flag= models.CharField(max_length=100, blank=True, null=True)
    codeCountry=models.CharField(max_length=20, blank=True, null=True)
    event=models.CharField(max_length=30, blank=True, null=True)

    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
        
    class Meta:
        db_table='Analytics'
        verbose_name='Analytics'
        verbose_name_plural='Analytics'
        ordering=['id']
            
# Create your models here.
class CodeAPIIP(models.Model):
    code = models.CharField(max_length=50, blank=True, null=True)
    date_created=models.DateTimeField(auto_now=True)
    date_update=models.DateTimeField(auto_now_add=True)
        
    class Meta:
        db_table='apiip'
        verbose_name='CodeAPIIP'
        verbose_name_plural='CodeAPIIPS'
        ordering=['id']

