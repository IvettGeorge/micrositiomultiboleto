from datetime import date
from django.contrib.auth import get_user_model, login
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from core.models import Event, Event_User
from django.http import HttpResponse, JsonResponse, request
from django.conf import Settings, settings


class EmailBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        codeEvent="300520214"
        try:
            user = UserModel.objects.get(email=username, code=codeEvent)
            print("Existe usuario")
            print(user)
        except UserModel.DoesNotExist:
            print("no existe usuario")
            return None
        else:
            if user.check_password(password):
                print(user.check_password(password))
                return user
        return None

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

class EmailEventUserBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        codeEvent=settings.CODE_EVENT   
        data={}     
        try:
            print(UserModel)
            try:
                event_user=Event_User.objects.get(mail=username, code=codeEvent)
                user= UserModel.objects.get(email=event_user.mail)
                print(user)
            except:
                data['error']='Usuario no autorizado'
                print('Usuario no autorizado')
                return None
                #return JsonResponse(data)                      
        except UserModel.DoesNotExist:
            data['error']='Este usuario no existe'
            print('Este usuario no existe')
            return None
            #return JsonResponse(data)
        else:
            if user.check_password(password):
                print(user.check_password(password))
                return user
            else:
                data['error']='Contraseña incorrecta'
                print('Contraseña incorrecta')
                return None
                #return JsonResponse(data)
        return None

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

class EmailEventUserMessagesBackend(BaseBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        codeEvent=settings.CODE_EVENT
        errors=[]
        try:
            try:
                event_user=Event_User.objects.get(mail=username, code=codeEvent)
                user= UserModel.objects.get(email=event_user.mail)
            except:
                errors.append('Usuario no autorizado..')
                print(errors)
                return None
                #return JsonResponse(data)                      
        except UserModel.DoesNotExist:
            errors.append('Usuario no existe..')
            print(errors)
            return None
            #return JsonResponse(data)
        else:
            if user.check_password(password):
                data="Inicio de sesión exitoso"                
                return user
            else:
                errors.append('Error de contraseña..')
                print(errors)
                return None
        return None

    def get_user(self, user_id):
        UserModel = get_user_model()
        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None
