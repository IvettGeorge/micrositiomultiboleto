from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput
from django.contrib.auth import authenticate

class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, required=True)

    def clean(self):
        username = self.cleaned_data.get('username')
        user = authenticate(username=username)
        if not user or not user.is_active:
            raise forms.ValidationError("Correo inválido. Intente de nuevo")
        return self.cleaned_data

    def login(self, request):
        username = self.cleaned_data.get('username')
        user = authenticate(username=username)
        return user