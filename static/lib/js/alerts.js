function muestraOK(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>Correcto:</strong>  ' + msg + '</div>');
    $("#alerta").show();
}

function muestraOKIndex(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>Correcto:</strong>  ' + msg + '<strong><a href = "login.html">\t    Ir a Inicio</a></strong></div>');
    $("#alerta").show();
}

function muestraOKIndex2(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="far fa-thumbs-up"></i><strong>Correcto:  ' + msg + '</strong><strong><a href = "login.html">\t    Ir a Inicio</a></strong></div>');
    $("#alerta").show();
}

function muestraOKIndex3(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="far fa-thumbs-up"></i><strong>  ' + msg + '</strong><strong> Link </a></strong></div>');
    $("#alerta").show();
}
function muestraAlerta(msg) {
    $('#alert_placeholder').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>Alerta:</strong>  ' + msg + '</div>');
    $("#alerta").show();
}

function muestraError(msg) {
    $('#alert_placeholder').html('<div class="alert alert-danger" id ="alerta"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ban-circle"></i><strong>Error:</strong> ' + msg + '</div>');
    $("#alerta").show();
}

function muestraMsg(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>  ' + msg + ' </strong></div>');
    $("#alerta").show();
}
function muestraAlertaMsg(msg) {
    $('#alert_placeholder').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>  ' + msg + '</strong></div>');
    $("#alerta").show();
}

function cleanMessage() {
    $('#alert_placeholder').html('');
    $("#alerta").hide();
}

function clearMessages() {
	jQuery('#alert_placeholder').html('');
}

function muestraMsgSurvey(msg) {
    $('#alert_placeholderSurvey').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>  ' + msg + ' </strong></div>');
    //$("#alerta").show();
}

function muestraErrorSurvey(msg) {
    $('#alert_placeholderSurvey').html('<div class="alert alert-danger" id ="alerta"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ban-circle"></i><strong>Error:</strong> ' + msg + '</div>');
    //$("#alerta").show();
}

function clearMessagesSurvey() {
	jQuery('#alert_placeholderSurvey').html('');
}

function muestraMsgAsistente(msg) {
    $('#alert_placeholderAsistente').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>  ' + msg + ' </strong></div>');
}

function muestraErrorAsistente(msg) {
    $('#alert_placeholderAsistente').html('<div class="alert alert-danger" ><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ban-circle"></i><strong>Error:</strong> ' + msg + '</div>');
}

function clearMessagesAsistente() {
	jQuery('#alert_placeholderAsistente').html('');
}


function muestraEnviado(msg) {
    $('#alert_placeholder').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="fa fa-ok-sign"></i><strong>  ' + msg + ' </strong></div>');
    $("#alerta").show();
}
