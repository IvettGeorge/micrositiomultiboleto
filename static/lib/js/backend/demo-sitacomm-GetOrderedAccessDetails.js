'use-strict'
const ASW = require('aws-sdk');
const documentClient = new ASW.DynamoDB.DocumentClient();


exports.handler = async (event, context) => {
    
    let responseBody = "";
    let statusCode = 0;

try {
        const params = {
            TableName: "demo_sitacomm_access_details",
            FilterExpression: " #activo = :vActivo",
            ExpressionAttributeNames: {           
                "#activo": "activo",
            },
            ExpressionAttributeValues: {           
                ":vActivo": true
            }
        };
        
        const paramsConnections = {
            TableName: "demo_sitacomm_access_connections",
        };
        var allDataConn=new Array;
        await getAllData(paramsConnections, allDataConn);
        
        
        var allData=new Array;
        await getAllData(params, allData);
        //console.log("Processing Completed");

        console.log("finallenght"+allData.length);
        let messages =allData.sort((a, b) => a.createdat - b.createdat);
       
        responseBody = //JSON.stringify(messages);
        {'items':JSON.stringify(messages),
           'connections':JSON.stringify(allDataConn)
        };
         statusCode = 200;
        //console.log(messages);
    } catch(error) {
        console.log(error);
       
        responseBody = error;
        statusCode = 403;
    }
    

    const response = {
        statusCode: statusCode,
        headers: {
            "Content-Type": "aplication/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(responseBody)
    };

    return response;
};



const getAllData = async (params, allData) => { 

    console.log("Querying Table");
    const data = await documentClient.scan(params).promise();

    if(data.Items){
        
       data.Items.forEach(function(item) {
          allData.push(item); 
       });
    }

   
    if (data.LastEvaluatedKey ) {
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        return await getAllData(params, allData);

    } else {
        return data;
    }
}
